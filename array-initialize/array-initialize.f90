program array_initialize
    implicit none
    
    integer :: i, j, limit
    real :: my_max
    real :: start, finish

    my_max = -1.0
    limit = 10000

    call cpu_time(start)
    do j=1, limit
        do i=1, limit
            my_max = max(my_max, initializer(i, j))
        end do
    end do
    call cpu_time(finish)

    print *, my_max
    print '("Time = ", f6.3," seconds.")', finish-start

contains
function initializer(i, j)
    implicit none
    real :: initializer
    real :: arr(2)
    integer :: i, j

    arr(1) = -1.0/(2*i+j+1)
    arr(2) = -1.0/(2*j+i+1)
    initializer = max(arr(1), arr(2))
end function
end program array_initialize

