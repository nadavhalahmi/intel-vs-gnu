# intel vs gnu

This project consists of a few simple examples to demonstrate intel vs gnu performance.

One may cd to any of the sub directories of this project and run the following commands:

`make gnu` to compile a gnu version of the example.

`make intel` to compile an intel version of the example.

Then, one may run `./gnu.out` or `./intel.out` to examine the run times.

One may change the compilation flags in `Makefile` and/or the example itself.

note: before running array-initialize example, one should look at its `Makefile`. 
