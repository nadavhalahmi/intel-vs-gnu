program exponent
    implicit none

    real :: sum, q, n
    integer :: i, j
    integer :: limit
    real :: start, finish

    sum = 0d0
    limit = 50000
    n = 2.0
    q = 0.5
    call CPU_TIME(start)
    do j=1, limit
        do i=1, limit
            n = n*q
            sum = (sum + exp(-5.0 + n))
        end do
    end do
    call CPU_TIME(finish)
    print *, sum
    print '("Time = ",f6.3," seconds.")',finish-start
end program exponent